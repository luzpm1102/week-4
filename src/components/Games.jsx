import React, { useState } from 'react';
import { useApi } from '../hooks/useApi';
import Game from './Game';
import Pagination from './Pagination';
import '../styles/games.scss';

export const Games = ({ setNavigation }) => {
  const { data } = useApi();
  const [currentPage, setCurrentPage] = useState(1);
  const gamesPerPage = 6;
  const indexOfTheLastGame = currentPage * gamesPerPage;
  const indexOfTheFirstGame = indexOfTheLastGame - gamesPerPage;
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const displayGames = data
    .slice(indexOfTheFirstGame, indexOfTheLastGame)
    .map(({ thumbnail, name, short_description, price, id }) => (
      <Game
        key={id}
        image={thumbnail}
        name={name}
        description={short_description}
        price={price}
        id={id}
        setNavigation={setNavigation}
      />
    ));

  return (
    <div>
      {data.length > 0 ? (
        <div>
          <div className='cards-container'>{displayGames}</div>
          <Pagination
            gamesPerPage={gamesPerPage}
            totalpages={data.length}
            paginate={paginate}
            currentPage={currentPage}
          />
        </div>
      ) : (
        <div className='loading-container'>
          <div className='loading'></div>
        </div>
      )}
    </div>
  );
};
