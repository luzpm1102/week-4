import React from 'react';
import { Comment } from './Comment';

export const Comments = ({ comments }) => {
  const displayComments = comments.map((comment) => (
    <Comment comment={comment} key={comment.id} />
  ));

  return <div>{displayComments}</div>;
};
