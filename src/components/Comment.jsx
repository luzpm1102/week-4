import React from 'react';
import '../styles/games.scss';

export const Comment = ({ comment }) => {
  return (
    <div className='comment-container'>
      <div className='comment__username'>
        <h5>{comment.user}</h5>
      </div>
      <hr />
      <div className='comment__body'>
        <p>{comment.body}</p>
      </div>
    </div>
  );
};
