import React from 'react';
import '../styles/footer.scss';

export const Footer = () => {
  return (
    <footer>
      <div className='footer'>
        <div className='footer__body'>
          <h5 className='footer__body__title'>© All rigths reserved, 2022</h5>
        </div>
      </div>
    </footer>
  );
};
