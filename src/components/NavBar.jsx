import React from 'react';
import logo from '../assets/img/game.png';
import '../styles/navbar.scss';

export const NavBar = ({ setNavigation }) => {
  return (
    <header>
      <div className='navbar'>
        <img className='navbar__logo' src={logo} alt='logo' />
        <div className='navbar__title' onClick={() => setNavigation(-1)}>
          <h1>Game Store</h1>
        </div>
        <div onClick={() => setNavigation(0)} className='navbar__title'>
          <h3 className='navbar__title--position'>Game List</h3>
        </div>
      </div>
    </header>
  );
};
