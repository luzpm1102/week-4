import React from 'react';
import '../styles/games.scss';

const Game = ({ image, name, description, price, id, setNavigation }) => {
  const ellipsify = (str) => {
    if (str.length > 10) {
      return str.substring(0, 35) + '...';
    } else {
      return str;
    }
  };
  return (
    <a onClick={() => setNavigation(id)}>
      <div className='card-container'>
        <div className='card-container__header'>
          <img src={image} alt='' />
        </div>
        <div className='card-container__body'>
          <h4>{name}</h4>
          <p>{ellipsify(description)}</p>
        </div>
        <div className='card-container__price'>
          <div className='card-container__price__price-text'>{'$' + price}</div>
        </div>
      </div>
    </a>
  );
};

export default Game;
