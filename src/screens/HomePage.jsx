import React from 'react';
import { Footer } from '../components/Footer';
import { NavBar } from '../components/NavBar';
import logo from '../assets/img/game.png';
import '../styles/homepage.scss';

export const HomePage = ({ setNavigation }) => {
  return (
    <div className='homepage-container'>
      <NavBar setNavigation={setNavigation} />
      <div className='mainpage-container'>
        <div className='title-container'>
          <h1 className='title'>WELCOME TO GAME STORE</h1>

          <h2>THE BEST PLACE FOR THE BEST GAMES</h2>
        </div>
        <div className='main-info'>
          <img src={logo} alt='logo' className='main-info__img' />
        </div>
      </div>

      <Footer />
    </div>
  );
};
