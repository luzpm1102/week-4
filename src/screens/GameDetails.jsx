import React, { useEffect, useState } from 'react';
import { Comments } from '../components/Comments';
import { Footer } from '../components/Footer';
import { NavBar } from '../components/NavBar';
import { useApi } from '../hooks/useApi';

export const GameDetails = ({ id, setNavigation }) => {
  const { getGameDetails } = useApi();
  const [game, setGame] = useState([]);

  useEffect(async () => {
    try {
      const result = await getGameDetails(id);
      setGame(result);
    } catch (error) {
      console.log(error);
    }
  }, [id]);

  return (
    <div className='home-container'>
      <NavBar setNavigation={setNavigation} />
      {game.id ? (
        <div className='main-container'>
          <div className='info-container'>
            <div className='game-details-container'>
              <div className='game-details__image-container'>
                <img
                  src={game.thumbnail}
                  alt={game.name}
                  className='image-container__img'
                />
              </div>
              <div className='game-details__body'>
                <h3>{game.name}</h3>
                <p>{game.short_description}</p>
                <h4>Platform: {game.platform}</h4>
                <h4>Publisher: {game.publisher}</h4>
                <h4>Genre: {game.genre}</h4>
                <button
                  className='game-details__price__button button-primary'
                  onClick={() => alert('You bougth ' + game.name)}
                >
                  <div className='game-details__price'>
                    <h4 className='game-details__price-text'>
                      {'Buy this game $' + game.price}
                    </h4>
                  </div>
                </button>
              </div>
            </div>

            <div className='game-comments'>
              <h3 className='game-comments__title'>Comments section</h3>
              {game.comments.length > 0 && (
                <Comments comments={game.comments} />
              )}
            </div>
            <div>
              <button
                onClick={() => setNavigation(0)}
                className='info__button button-primary'
              >
                Go Back
              </button>
            </div>
          </div>
        </div>
      ) : (
        <div className='loading-container'>
          <div className='loading'></div>
        </div>
      )}
      <Footer />
    </div>
  );
};
