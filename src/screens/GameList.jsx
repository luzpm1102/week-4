import React from 'react';
import { Footer } from '../components/Footer';
import { Games } from '../components/Games';
import { NavBar } from '../components/NavBar';

export const GameList = ({ setNavigation }) => {
  return (
    <div className='home-container'>
      <NavBar setNavigation={setNavigation} />
      <div className='main-container'>
        <div className='header-title-container'>
          <h1 className='header-title'>Games available</h1>
        </div>
        <Games setNavigation={setNavigation} />
      </div>

      <Footer />
    </div>
  );
};
