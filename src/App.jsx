import { useState } from 'react';
import './App.css';
import { GameDetails } from './screens/GameDetails';
import { GameList } from './screens/GameList';
import { HomePage } from './screens/HomePage';
import './styles/styles.scss';

const App = () => {
  const [navigation, setNavigation] = useState(0);

  const changeId = (id) => setNavigation(id);
  return (
    <div className='App'>
      {navigation === -1 && <HomePage setNavigation={changeId} />}
      {navigation === 0 && <GameList setNavigation={changeId} />}
      {navigation > 0 && (
        <GameDetails id={navigation} setNavigation={setNavigation} />
      )}
    </div>
  );
};

export default App;
