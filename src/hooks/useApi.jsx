import React, { useEffect, useState } from 'react';

const uri = `https://server-week-4.herokuapp.com/games`;
export const useApi = () => {
  const [data, setData] = useState([]);

  useEffect(async () => {
    const result = await fetch(uri)
      .then((res) => res.json())
      .catch((error) => console.log(error));
    setData(result);
  }, []);
  const getGameDetails = async (id) => {
    const result = await fetch(`${uri}/${id}`)
      .then((res) => res.json())
      .catch((error) => console.log(error));
    return result;
  };

  return {
    data,
    getGameDetails,
  };
};
